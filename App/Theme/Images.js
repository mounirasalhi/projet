/**
 * Images should be stored in the `App/Images` directory and referenced using variables defined here.
 */

export default {

  nobo: require('App/Assets/Images/nobo.png'),
  ic_accessibility_white: require('App/Assets/Images/ic_accessibility_white.png'),
  ic_language_white: require('App/Assets/Images/ic_language_white.png'),
  ic_room_white: require('App/Assets/Images/ic_room_white.png'),
  no_data : require('App/Assets/Images/nodata.png'),
}
