import React from 'react';
import { Text, View, RefreshControl, FlatList, ActivityIndicator, Image } from 'react-native';
import styles from './MaidsStyle';
import { ScrollView } from 'react-navigation';
import { Avatar, Button, Card, Divider } from 'react-native-paper';
import { FloatingAction } from 'react-native-floating-action';
import { Images } from 'App/Theme';
import  CardFlatList  from 'App/Components/Cards/CardFlatList';

const actions = [
    {
        text: 'Accessibility',
        icon: Images.ic_accessibility_white,
        name: 'bt_accessibility',
        position: 2,
    },
    {
        text: 'Language',
        icon: Images.ic_language_white,
        name: 'bt_language',
        position: 1,
    },
    {
        text: 'Location',
        icon: Images.ic_room_white,
        name: 'bt_room',
        position: 3,
    },
];
const data =[];
const listEmptyComponent = () => {
    return (
        <View>
            <View style={[styles.greyRectangle, { backgroundColor: 'red' }]}></View>
            <View style={{ alignItems: 'center' }}>
                <Image resizeMode="contain" style={{ width: 200, height: 200 }} source={Images.no_data} />
                <Text style={{ marginTop: 10 }}>Aucune donnée disponible</Text>
            </View>
        </View>
    );
};

const LeftContent = (props) => <Avatar.Icon {...props} size={36} icon="folder" />;

const SecondRoute = () => {
    return (
        <View style={styles.scene}>
            <Card style={{ borderRadius: 20, boxShadow: 2, flex: 1 }}>
                <View style={{ flex: 1 }}>
                    <FlatList
                        ListEmptyComponent={listEmptyComponent}
                        contentContainerStyle={{ paddingLeft: 10, paddingRight: 10 }}
                        data={data}
                        renderItem={({ item, index }) => {
                            return !index ? (
                                <View>
                                    <View style={styles.greyRectangle}></View>
                                    <CardFlatList data={item} />
                                    <Divider />
                                </View>
                            ) : (
                                <View>
                                    <CardFlatList data={item} />
                                    <Divider />
                                </View>
                            );
                        }}
                        keyExtractor={(item) => item.prestation_id.toString()}
                    />
                </View>

                <FloatingAction
                    actions={actions}
                    onPressItem={(name) => {
                        console.log(`selected button: ${name}`);
                    }}
                />
            </Card>
        </View>
    );
};
export default SecondRoute;
