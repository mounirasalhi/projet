import React from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';

import SplashScreen from 'App/Containers/SplashScreen/SplashScreen';
import { createDrawerNavigator } from 'react-navigation';
import Menu from './Menu';
import DrawerItem from './DrawerItems/DrawerItem';
import MaidsStack from './RoutesStacks/MaidsStack';

const routeMap = {

  Maids: {
    screen: MaidsStack,
    navigationOptions: (navOpt) => ({
      drawerLabel: ({ focused }) => <DrawerItem focused={focused} title="Maids" />,
    }),
  },
};

const DrawerNavigator = createDrawerNavigator(routeMap, Menu);

const StackNavigator = createStackNavigator(
  {
    SplashScreen: SplashScreen,
    MainScreen: DrawerNavigator,
  },
  {
    initialRouteName: 'SplashScreen',
    headerMode: 'none',
  }
);

export default createAppContainer(StackNavigator);
