import React from "react";
import {createStackNavigator} from "react-navigation";
import Maids from 'App/Containers/Maids/Maids'
import Header from "App/Components/Header/Header"


const MaidsStack = createStackNavigator({
    MaidsStack: {
        screen: Maids,
        navigationOptions: ({ navigation }) => {
            return {
                header: () => (
                    <Header title="Maids" hasBackButton={false} navigation={navigation} />
                )
            };
        }
    }
});
export default MaidsStack
