import React, { Component } from 'react'
import RootScreen from './Containers/Root/RootScreen'
import { Provider as PaperProvider } from 'react-native-paper';
import { ApplicationStyles } from 'App/Theme'




export default class App extends Component {
    render() {
        return (


                    <PaperProvider theme={ApplicationStyles.paperStyle}>
                        <RootScreen />
                    </PaperProvider>

        )
    }
}
